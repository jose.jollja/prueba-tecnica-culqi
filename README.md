# AWS serverless payment

## Dependencies packages

- [zod](https://www.npmjs.com/package/zod)---TypeScript-first schema validation with static type inference
- [dotenv](https://www.npmjs.com/package/dotenv)---Dotenv is a zero-dependency module that loads environment variables
- [ioredis](https://www.npmjs.com/package/ioredis)---A robust, performance-focused and full-featured Redis client for Node.js.

## Dev-Dependency packages

- [@serverless/typescript](https://www.npmjs.com/package/@serverless/typescript) --- Typescript definitions for Serverless `serverless.ts` service file.
- [@types/aws-lambda](https://www.npmjs.com/package/@types/aws-lambda) --- Typescript type definitions for AWS Lambda
- [@types/jest](https://www.npmjs.com/package/@types/jest) --- Typescript type definitions for Jest
- [@types/node](https://www.npmjs.com/package/@types/node) --- TypeScript definitions for Node.js
- [esbuild](https://www.npmjs.com/package/esbuild) --- Javascript/Typescript bundler
- [eslint](https://www.npmjs.com/package/eslint) --- Javascript linter tool
- [eslint-config-airbnb-base ](https://www.npmjs.com/package/eslint-config-airbnb-base)--- Airbnb's base JS ESLint config
- [@typescript-eslint/eslint-plugin](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin) --- TypeScript plugin for ESLint
- [@typescript-eslint/parser](https://www.npmjs.com/package/@typescript-eslint/parser) --- ESLint custom parser which leverages TypeScript ESTree
- [jest](https://www.npmjs.com/package/jest) --- Testing tool
- [ts-jest](https://www.npmjs.com/package/ts-jest) - Jest Typescript transformer
- [ts-node](https://www.npmjs.com/package/ts-node) --- TypeScript execution environment and REPL for node.js, with source map support
- [tsconfig-paths](https://www.npmjs.com/package/tsconfig-paths) --- Load node modules according to tsconfig paths, in run-time or via API.
- [typescript](https://www.npmjs.com/package/typescript) --- TypeScript is a language for application scale JavaScript development

## Installation instructions

- Run `npm install` to install the project dependencies

## Locally

Create the .env file in the project root with the following variables to be able to connect to your redis service

```
REDIS_PORT=
REDIS_HOST=
REDIS_PASSWORD=
NODE_ENV=
```

Run the following command:

- `npm run start`

If you can see something like `Server ready: http://localhost:4000 🚀` this means the local service is ready.

## Run unit test

- Run `npm run test` to run all unit test
- Run `npm run test:coverage` to run the test and generate a coverage report

## Deploy

- Have aws-cli installed on your computer, read the following documentation [aws-cli](https://aws.amazon.com/es/cli/).

- Configure aws-cli credentials, [docs](https://docs.aws.amazon.com/cli/latest/reference/configure/index.html).

- Run `npm run deploy` to deploy the app.
