// Validar las variables de entorno
import 'src/config'
import { manageCards } from './src/schema/card'
import type { AWS } from '@serverless/typescript'

const serverlessConfiguration: AWS = {
  service: 'aws-lambda-payment',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild', 'serverless-offline'],
  provider: {
    name: 'aws',
    region: 'us-west-2',
    runtime: 'nodejs18.x',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true
    },
    environment: {
      REDIS_PORT: process.env.REDIS_PORT ?? '',
      REDIS_HOST: process.env.REDIS_HOST ?? '',
      REDIS_PASSWORD: process.env.REDIS_PASSWORD ?? '',
      NODE_ENV: process.env.NODE_ENV ?? '',
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000'
    }
  },
  functions: {
    manageCards
  },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: 'inline',
      target: 'node18',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
      tsconfig: 'tsconfig.build.json',
      watch: {
        pattern: ['src/**/*.ts'],
        ignore: ['src/**/*.spec.ts', 'src/**/*.router.ts']
      }
    },
    'serverless-offline': {
      noAuth: true,
      httpPort: 4000,
      noPrependStageInUrl: true
    }
  }
}

module.exports = serverlessConfiguration
