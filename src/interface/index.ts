export const enum ACTION_CODE {
  Successful = '00',
  Failed = '01'
}

export const enum MESSAGE_TYPE {
  RequestAuthorization = '0200',
  ResponseAuthorization = '0210',
  RequestReversion = '0400',
  ResponseReversion = '0410',
  RequestTokenization = '0300',
  ResponseTokenization = '0310'
}

export interface BaseReponse {
  message_type: MESSAGE_TYPE
  action_code: ACTION_CODE
  action_description: string
}
