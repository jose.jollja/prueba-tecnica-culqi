import AppError from './app.error'
import { formatJSONResponse } from './helpers'
import type {
  APIGatewayProxyHandler,
  APIGatewayProxyEvent,
  Context
} from 'aws-lambda'

export interface ICustomAPIGatewayProxyEvent<TBody = unknown>
  extends Omit<APIGatewayProxyEvent, 'body'> {
  body: TBody
}

export interface ICustomAPIGatewayProxyHandler<
  TBody = unknown,
  TResult = unknown
> {
  (event: ICustomAPIGatewayProxyEvent<TBody>): Promise<TResult>
}

export interface ICustomAPIGatewayProxyMiddleware<TBody = unknown> {
  (
    event: ICustomAPIGatewayProxyEvent<TBody>,
    ctx: Context,
    next: () => Promise<unknown>
  ): Promise<unknown>
}

export interface IRouter {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'
  resource: string
  handler: ICustomAPIGatewayProxyHandler
  middleware?: ICustomAPIGatewayProxyMiddleware
}

export function createAPIGatewayProxyHandler(
  routers: IRouter[]
): APIGatewayProxyHandler {
  const mapRouter = new Map<string, IRouter>()
  for (const r of routers) mapRouter.set(`${r.method}@${r.resource}`, r)

  return async (event, ctx) => {
    try {
      const { httpMethod, resource } = event
      const foundRouter = mapRouter.get(`${httpMethod}@${resource}`)

      if (!foundRouter) {
        return formatJSONResponse(
          { message: `${httpMethod.toUpperCase()} ${resource} not found!` },
          404
        )
      }

      if (typeof foundRouter?.middleware === 'function') {
        const result = await foundRouter.middleware(event, ctx, async () => {
          return await foundRouter.handler({
            ...event,
            body: event.body ? JSON.parse(event.body) : {}
          })
        })

        return formatJSONResponse(result)
      }

      const result = await foundRouter.handler({
        ...event,
        body: event.body ? JSON.parse(event.body) : {}
      })

      return formatJSONResponse(result)
    } catch (e: unknown) {
      console.error(e)

      const error = e as Error
      let statusCode = 500

      if (error instanceof AppError) {
        statusCode = error.options.statusCode ?? 400
      }

      return formatJSONResponse({ message: error.message }, statusCode)
    }
  }
}
