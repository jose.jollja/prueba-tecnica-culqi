interface AppErrorOptions {
  statusCode?: number
}

export default class AppError extends Error {
  options: AppErrorOptions = {
    statusCode: 400
  }

  constructor(message: string, options?: AppErrorOptions) {
    super(message)
    this.name = this.constructor.name
    this.options = { ...this.options, ...options }
  }
}
