import * as func from './generate-token'

describe('generateToken()', () => {
  it('should return a string with 16 characters', async () => {
    const token = '05cedffd944b43d5'

    jest
      .spyOn(func, 'generateToken')
      .mockName('func.generateToken')
      .mockResolvedValue(token as never)

    const actual = await func.generateToken()
    expect(actual).toBe(token)
  })
})
