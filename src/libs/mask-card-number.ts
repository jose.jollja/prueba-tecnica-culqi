export function maskCardNumber(cardNumber: string) {
  return cardNumber.replace(/^[\d-\s]+(?=\d{4})/, '************')
}
