import config from '@src/config'
import { Redis } from 'ioredis'

const { port, host, password } = config.cache

export default new Redis(`redis://:${password}@${host}:${port}`)
