import type { ICustomAPIGatewayProxyMiddleware } from '@src/libs/api-gateway'
import AppError from '@src/libs/app.error'

export const checkJWT: ICustomAPIGatewayProxyMiddleware = async (
  event,
  _ctx,
  next
) => {
  const authorization = event.headers?.Authorization ?? null

  if (!authorization) {
    throw new AppError('Invalid token')
  }

  const isValidToken = authorization.includes('Bearer')

  if (!isValidToken) {
    throw new AppError('Invalid token')
  }

  // const payload = await jwt.decode(authorization.replace('Bearer ', ''))
  // const user = await db.getUser({ id: payload.id })
  // event.requestContext.user = user

  return await next()
}
