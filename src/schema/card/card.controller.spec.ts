import * as controller from './card.controller'
import type { ICustomAPIGatewayProxyEvent } from '@src/libs/api-gateway'
import type {
  ICreateCardInput,
  ICreateCardResponse,
  IGetCardByTokenResponse,
  IGetCardByTokenInput
} from './card.model'
import redis from '@src/libs/redis'

describe('CardController', () => {
  afterAll((done) => {
    redis.disconnect()
    done()
  })

  describe('cardTokenization()', () => {
    it('should return a token', async () => {
      const newCard = {
        action_code: '00',
        message_type: '0310',
        action_description: 'Passed and successfully completed',
        data: { token: '05cedffd944b43d5' }
      } as ICreateCardResponse

      const dto = {
        email: 'admin@gmail.com',
        card_number: '4111111111111111',
        cvv: '123',
        expiration_year: '2023',
        expiration_month: '11'
      } as ICreateCardInput

      const event = {
        body: dto
      } as ICustomAPIGatewayProxyEvent<ICreateCardInput>

      jest
        .spyOn(controller, 'cardTokenization')
        .mockName('controller.cardTokenization')
        .mockResolvedValue(newCard)

      const actual = await controller.cardTokenization(event)

      expect(controller.cardTokenization).toHaveBeenCalledWith(event)
      expect(actual).toBe(newCard)
    })
  })

  describe('getCardByToken()', () => {
    it('should return a card', async () => {
      const card = {
        action_code: '00',
        message_type: '0310',
        action_description: 'Passed and successfully completed',
        data: {
          email: 'admin@gmail.com',
          card_number: '4111111111111111',
          expiration_year: '2023',
          expiration_month: '11'
        }
      } as IGetCardByTokenResponse

      const dto = {
        token: '05cedffd944b43d5'
      } as IGetCardByTokenInput

      const event = {
        body: dto
      } as ICustomAPIGatewayProxyEvent<IGetCardByTokenInput>

      jest
        .spyOn(controller, 'getCardByToken')
        .mockName('controller.getCardByToken')
        .mockResolvedValue(card)

      const actual = await controller.getCardByToken(event)

      expect(controller.getCardByToken).toHaveBeenCalledWith(event)
      expect(actual).toBe(card)
    })
  })
})
