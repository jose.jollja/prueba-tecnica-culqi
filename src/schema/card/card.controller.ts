import { z } from 'zod'

import {
  cardSchema,
  type ICard,
  type ICreateCardInput,
  type ICreateCardResponse,
  type IGetCardByTokenResponse,
  type IGetCardByTokenInput
} from './card.model'

import redis from '@src/libs/redis'
import { generateToken } from '@src/libs/generate-token'
import type { ICustomAPIGatewayProxyEvent } from '@src/libs/api-gateway'
import { ACTION_CODE, MESSAGE_TYPE } from '@src/interface'

export async function cardTokenization(
  event: ICustomAPIGatewayProxyEvent<ICreateCardInput>
): Promise<ICreateCardResponse> {
  const parsed = cardSchema.safeParse(event.body)

  if (!parsed.success) {
    return {
      message_type: MESSAGE_TYPE.ResponseTokenization,
      action_code: ACTION_CODE.Failed,
      action_description: parsed.error.errors?.[0].message ?? 'Unknown error'
    }
  }

  const token = generateToken()
  await redis.set(token, JSON.stringify(event.body), 'EX', 15 * 60)

  return {
    message_type: MESSAGE_TYPE.ResponseTokenization,
    action_code: ACTION_CODE.Successful,
    action_description: 'Passed and successfully completed',
    data: { token }
  }
}

export async function getCardByToken(
  event: ICustomAPIGatewayProxyEvent<IGetCardByTokenInput>
): Promise<IGetCardByTokenResponse> {
  const schema = z.object({ token: z.string().length(16) })
  const parsed = schema.safeParse(event.body)

  if (!parsed.success) {
    return {
      message_type: MESSAGE_TYPE.ResponseAuthorization,
      action_code: ACTION_CODE.Failed,
      action_description: 'Invalid Token'
    }
  }

  const card = await redis.get(event.body.token).then((json) => {
    if (typeof json !== 'string') return null
    return JSON.parse(json) as ICard
  })

  if (card === null) {
    return {
      message_type: MESSAGE_TYPE.ResponseAuthorization,
      action_code: ACTION_CODE.Failed,
      action_description: 'Invalid Token'
    }
  }

  return {
    message_type: MESSAGE_TYPE.ResponseAuthorization,
    action_code: ACTION_CODE.Successful,
    action_description: 'Passed and successfully completed',
    data: {
      email: card.email,
      card_number: card.card_number,
      expiration_year: card.expiration_year,
      expiration_month: card.expiration_month
    }
  }
}
