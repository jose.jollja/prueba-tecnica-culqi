import { z } from 'zod'
import { luhnCheck } from '@src/libs/luhn-check'
import { BaseReponse } from '@src/interface'

export const cardSchema = z
  .object({
    email: z
      .string()
      .email()
      .refine(
        (data) => {
          return (
            data.includes('gmail.com') ||
            data.includes('hotmail.com') ||
            data.includes('yahoo.com')
          )
        },
        { message: 'Incorrect email domain' }
      ),

    cvv: z.union([
      z.string().length(3, { message: 'CVV must contain 3 or 4 characters' }),
      z.string().length(4, { message: 'CVV must contain 3 or 4 characters' })
    ]),

    card_number: z
      .string()
      .length(16, { message: 'Invalid length card number' })
      .refine((data) => luhnCheck(data), { message: 'Invalid card number' }),

    expiration_year: z
      .string()
      .length(4, { message: 'Year must contain exactly 4 digits' })
      .refine(
        (data) => {
          const now = new Date()
          const year = now.getFullYear()
          return (
            !isNaN(Number(data)) &&
            Number(data) >= year &&
            Number(data) <= year + 5
          )
        },
        { message: 'Invalid year' }
      ),

    expiration_month: z.string().refine(
      (data) => {
        return !isNaN(Number(data)) && Number(data) >= 1 && Number(data) <= 12
      },
      { message: 'Invalid month' }
    )
  })
  .refine(
    (obj) => {
      const now = new Date()
      return Number(obj.expiration_year) === now.getFullYear()
        ? Number(obj.expiration_month) >= now.getMonth()
        : true
    },
    { message: 'Card expire' }
  )

export type ICard = z.infer<typeof cardSchema>

export type ICreateCardInput = ICard
export interface ICreateCardResponse extends BaseReponse {
  data?: { token: string }
}

export type IGetCardByTokenInput = { token: string }
export type IGetCardByTokenResponse = BaseReponse & {
  data?: Omit<ICard, 'cvv'>
}
