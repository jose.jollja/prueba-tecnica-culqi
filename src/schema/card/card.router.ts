import * as auth from '@src/middlewares/auth'
import * as controller from './card.controller'

import { ROOT_PATH } from '@src/libs/constanst'
import type { IRouter } from '@src/libs/api-gateway'

const routers: IRouter[] = [
  {
    method: 'GET',
    resource: '/',
    handler: async () => ({ message: 'Is Working' })
  },
  {
    method: 'POST',
    resource: `${ROOT_PATH}/tokens`,
    middleware: auth.checkJWT,
    handler: controller.cardTokenization
  },
  {
    method: 'POST',
    resource: `${ROOT_PATH}/charges`,
    middleware: auth.checkJWT,
    handler: controller.getCardByToken
  }
]

export default routers
