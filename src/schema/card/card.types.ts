export const enum TOKENIZATION_STATUS_CODE {
  Successful = '911000',
  Failed = '912000',
  CardNotFound = '913000',
  CardExpire = '914000',
  CardBlocked = '915000'
}
export const enum TOKENIZATION_RESPONSE_CODE {
  Successful = '00',
  Failed = '01',
  CardNotFound = '02',
  CardExpire = '03',
  CardBlocked = '04',
  CardNotSupported = '05'
}

export const enum CURRENCY_CODE {
  PEN = '604',
  USD = '840'
}
