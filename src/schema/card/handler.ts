import routers from './card.router'
import { createAPIGatewayProxyHandler } from '@src/libs/api-gateway'

export const main = createAPIGatewayProxyHandler(routers)
