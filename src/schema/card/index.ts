import routers from './card.router'
import { handlerPath } from '@src/libs/handler-resolver'

export const manageCards = {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: routers.map((router) => ({
    http: { path: router.resource, method: router.method }
  }))
}
